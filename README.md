# Hackthebox Openadmin

### Escaneo de Puertos

    Nmap scan report for 10.10.10.171

    Host is up (0.20s latency).

    Not shown: 65501 closed ports

    PORT      STATE    SERVICE       VERSION

    22/tcp    open     ssh           OpenSSH 7.6p1 Ubuntu 4ubuntu0.3 (Ubuntu Linux; protocol 2.0)

    | ssh-hostkey: 

    |   2048 4b:98:df:85:d1:7e:f0:3d:da:48:cd:bc:92:00:b7:54 (RSA)

    |   256 dc:eb:3d:c9:44:d1:18:b1:22:b4:cf:de:bd:6c:7a:54 (ECDSA)

    |_  256 dc:ad:ca:3c:11:31:5b:6f:e6:a4:89:34:7c:9b:e5:50 (ED25519)

    80/tcp    open     http          Apache httpd 2.4.29 ((Ubuntu))

    | http-methods: 

    |_  Supported Methods: HEAD GET POST OPTIONS

    |_http-title: Apache2 Ubuntu Default Page: It works


### Enumeracion de directorios

Listado de directorios de Openadmin con Owasp Dirbuster:

![alt text](img/OWASP_DIrbuster.png "Dirbuster")

### Identificacion de Vulnerabilidades

Ya que OpenSSH esta es su version mas actualizada procedimos a analizar manualmente los directiorios de Openadmin

Encontramos las siguientes paginas con contenido interesante :

![alt text](img/ona_login.png "Ona_login")

En internet encontramos un exploit que permite la ejecucion remota de codigo aprovechando uno de los parametros de la peticion POST del login,Procedemos a ejecutarlos 

Este es el exploit [OpenNetAdmin 18.1.1 - Remote Code Execution](https://www.exploit-db.com/exploits/47691)

Ejecutando el exploit :

![alt text](img/open_remote_code.png "Exploit")

Euereka obtuvimos una reverse shell pero es muy simple a continuacion subire una mejor [shell-reversa-by-pentesmonkey](https://github.com/pentestmonkey/php-reverse-shell)

### Escalando Privilegios

En la reverse shell ejecutamos [linpeas](https://github.com/carlospolop/privilege-escalation-awesome-scripts-suite/tree/master/linPEAS) para encontrar los posibles vectores que me permitan escalar los privilegios 
>>>
 $ chmod +x linpeas.sh	
>>>
>>>
 $ ./linpeas.sh
>>>
En el reporte de linpeas obtimos  la siguiente contraseña 

![alt text](img/Contraseña_by_linpeas.png "Contraseña")

Probamos iniciar sesion en ssh con la contraseña que obtuvimos

![alt text](img/Sesion_ssh_with_jimmy.png "SSH_sesion")

good obtuvimos una sesion con el usuario jimmy

Ahora para poder escalar de privilegios del usuario jimmy  a la usuario joanna

Hare una peticion GET al siguiente url http://localhost:52846/main.php obteniendo :

![alt text](img/RSA_of_joanna.png "SSH_of_joanna")

Ahora al parecer la private_key tiene frase de seguridad asi que procedemos a crakearla con john 

Pasando la private_key a un documento texto con [ssh2john](https://github.com/koboi137/john/blob/bionic/ssh2john.py) 

![alt text](img/rsa_to_txt.png "crack.txt")

Usando [john the ripper](https://github.com/magnumripper/JohnTheRipper) para crakear la private_key

![alt text](img/john_the_ripper.png "craked")

Bien ahora ya podemos iniciar sesion con el usuario joanna

Ahora escalemos privlegios ejecutamos sudo -l para ver que programas pueden ejecutarse como root
![alt text](img/sudo_l.png "sudo_l")

Ajá,Ejecutamos sudo /bin/nano /opt/priv y nos encontramos con el entorno de nano ahora ejecutamos CTRL+R CTRL+X y probamos con el comando whoami para ver que usuario somos 
![alt text](img/exec_comand_in_nano.png "exec_comand")

![alt text](img/root.png "Poderosisimo root :)")

ok entonces spawneemos una shell para tener una shell como root

![alt text](img/spawning_shell_nano.png "spwan_shell")


![alt text](img/shell_root.png "spwan_shell")

Mision complete :))